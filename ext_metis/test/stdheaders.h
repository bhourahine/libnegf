/*
 * Copyright 1997, Regents of the University of Minnesota
 *
 * stdheaders.h
 *
 * This file includes all necessary header files
 *
 * Started 8/27/94
 * George
 *
 * $Id: stdheaders.h 2501 2007-11-20 02:33:29Z benkirk $
 */


#include <stdio.h>
#ifdef __STDC__
#include <stdlib.h>
#elif defined(__APPLE__)
#include <stdlib.h>
#else
#include <malloc.h>
#endif
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <stdarg.h>
#include <time.h>

